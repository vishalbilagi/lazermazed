﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Unlocker : MonoBehaviour {

    public bool[] dirsToAccept = { false, false, false, false };
    public int[] updfls = new int[4];
    public int i;
    //for intermediate plates↓
    public bool unlockerObject;
    //for final destination↓
    public ParticleSystem par;
    public AirCastSource airStop;
    public int sceneNo;



    void Start()
    {
        airStop = GameObject.FindGameObjectWithTag("source").GetComponent<AirCastSource>();

        if (GetComponent<ParticleSystem>())
        {
            par = GetComponent<ParticleSystem>();
            par.emissionRate = 0;
            unlockerObject = false;
            
        }
        else
        {
            unlockerObject = true;
        }

        TimeDelayUpdate();
    }

    
    void LateUpdate()
    {
        if (dirsToAccept[0] == true && dirsToAccept[1] == true && dirsToAccept[2] == true  && dirsToAccept[3] == true)
        {
            if (unlockerObject)
            {
                Debug.Log(gameObject.name + " destroyed");
                transform.parent.gameObject.SetActive(false);//Plate is set inactive along with its child collider
            }
            else
            {
                StartCoroutine("Wait"); //←uncomment this
                //TEST PURPOSES↓
               // Debug.Log("Puzzle Solved");
                //airStop.stop = true;
                //par.Emit(300);

            }

        }
        
    }

    IEnumerator Wait()
    {
        par.Emit(300);
        yield return new WaitForSeconds(1);
        Debug.Log("Puzzle Solved");
        airStop.stop = true;
        SceneManager.LoadScene(sceneNo);
    }
    //Reset incoming rays every 0.1 unit time so that all the rays get accepted at the same time
    void TimeDelayUpdate()
    {
        for (i = 0; i < 4; i++)
        {
            if (updfls[i] != 1)
            {
                dirsToAccept[i] = false;
            }
        }

        Invoke("TimeDelayUpdate", 0.1f);
    }

}
