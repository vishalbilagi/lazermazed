﻿using UnityEngine;
using System.Collections;

public class HitElbow : MonoBehaviour {

    public RaycastHit hitInfo;

    /* USAGE : add the objects that are either destination or 
       unlock plates to unlocker script reference object in the inspector panel
    */
    public Unlocker unlocker;
    public int[] noRays = new int[4];
    public int i;
    public Transform pos;
    public LineRenderer pipeRay;

    public bool flag = false, rot = false;

    public Vector3[] emitDir = {new Vector3(0, 0, 10),//up
                                new Vector3(0, 0, -10),//down
                                new Vector3(-10, 0, 0),//left
                                new Vector3(10, 0, 0)};//right

    void Start()
    {
        //initialize script to detect whether this gameobject is going to hit a destination target or a sphere unlocker
        if (!unlocker)
        {
            flag = true;
        }

        for (int i = 0; i < 4; i++)
            noRays[i] = 0;

        pos = GetComponent<Transform>();
        pipeRay = GetComponent<LineRenderer>();
        pipeRay.material = new Material(Shader.Find("Unlit/Color"));
        pipeRay.material.SetColor("_Color", Color.red);
        pipeRay.SetColors(Color.red, Color.red);
        pipeRay.SetPosition(0, pos.transform.position);
        pipeRay.enabled = false;

       
    }

    void Update()
    {
        
        if (rot)
        {
            if ((int)transform.eulerAngles.y == 0 || (int)transform.eulerAngles.y < 90)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 91, 0), 10f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 90) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 90 || (int)transform.eulerAngles.y < 180)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 181, 0), 10f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 180) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 180 || (int)transform.eulerAngles.y < 270)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 271, 0), 10f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 270) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 270 || (int)transform.eulerAngles.y < 360)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 1, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 0) rot = false;
            }

        }
    }

    void DisableEmission()
    {
        pipeRay.enabled = false;

        if (hitInfo.transform && hitInfo.collider.tag != "bound" && hitInfo.collider.tag != "dest" && hitInfo.collider.tag != "blocker")
            hitInfo.transform.SendMessage("DisableEmission");

    }

    void Rotate()
    {
        rot = true;
        
        if(hitInfo.transform && hitInfo.collider.tag != "bound" && hitInfo.collider.tag != "dest" && hitInfo.collider.tag != "blocker") 
            hitInfo.transform.SendMessage("DisableEmission");

        pipeRay.enabled = false;
    }

    //check only the object that has a "DirectionOfRay" function receiver, they are pipe, elbow and tjoint 
    //OR destination or unlocker that needs dirsToAccept[] to be set to true in case if a ray from this gameobject hits it
    //(all hits are taken at destination or unlocker as directon of this gameobject's emission of ray 
    // OR in simple words, just the opposite direction)

    void checkValidObject(RaycastHit hitInfo, Vector3 dir)
    {

        pipeRay.SetPosition(1, hitInfo.point);
        pipeRay.enabled = true;

        if (hitInfo.collider.tag == "pipe" || hitInfo.collider.tag == "elbow"|| hitInfo.collider.tag == "tjoint")
            hitInfo.transform.SendMessage("DirectionOfRay", dir);
        if ((hitInfo.collider.tag == "unlocker" || hitInfo.collider.tag == "dest") && !flag )
        {
            if (dir == new Vector3(0, 0, 10))
                unlocker.dirsToAccept[0] = true;
            else if (dir == new Vector3(0, 0, -10))
                unlocker.dirsToAccept[1] = true;
            else if (dir == new Vector3(-10, 0, 0))
                unlocker.dirsToAccept[2] = true;
            else if (dir == new Vector3(10, 0, 0))
                unlocker.dirsToAccept[3] = true;
        }
        
        if((hitInfo.collider.tag == "unlocker" || hitInfo.collider.tag =="dest") && flag){
            Debug.Log("You may want to assign an Unlocker gameobject to " + this.gameObject.name);
        }
        if(hitInfo.collider.tag == "blocker")
        {
            hitInfo.transform.parent.transform.SendMessage("LoopCheck");
        }

    }

    void LoopCheck()
    {
        for(i = 0; i < 4; i++)
        {
            noRays[i] = 0;
        }
    }

    void DirectionOfRay(Vector3 ray)
    {

        if (ray == emitDir[0])
            noRays[0] = 1;
        else if (ray == emitDir[1])
            noRays[1] = 1;
        else if (ray == emitDir[2])
            noRays[2] = 1;
        else if (ray == emitDir[3])
            noRays[3] = 1;

        if (noRays[0] + noRays[1] + noRays[2] + noRays[3] == 1)
        {

            //Horizontal incoming rays
            if (ray.x == 10 || ray.x == -10)
            {

                /*
                *   270 d
                *   incoming ray →[1][e]
                *                    [0]
                *                     ↓
                *                     outgoing ray
                */
                if ((int)transform.eulerAngles.y == 270)
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[1], out hitInfo))
                    {
                        
                        Debug.DrawRay(transform.position, emitDir[1], Color.red);
                        checkValidObject(hitInfo, emitDir[1]);
                    }

                }
                /*
                *   180d
                *   [e][0]← incoming ray
                *   [1]
                *    ↓
                *    outgoing ray
                */
                else if ((int)transform.eulerAngles.y == 180)
                {
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[1], out hitInfo))
                    {
                        Debug.DrawRay(transform.position, emitDir[1], Color.red);
                        checkValidObject(hitInfo, emitDir[1]);
                    }
                }
                /*
                *    0d
                *                   outgoing ray
                *                   ↑
                *                  [1] 
                *  incoming ray→[0][e]
                */
                else if ((int)transform.eulerAngles.y == 0)
                {
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[0], out hitInfo))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[0], Color.red);
                        checkValidObject(hitInfo, emitDir[0]);
                    }
                }
                /*
                *   90d
                *   outgoing ray
                *    ↑
                *   [0]
                *   [e][1]← incoming ray
                */
                else if ((int)transform.eulerAngles.y == 90)
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[0], out hitInfo))
                    {
                        Debug.DrawRay(transform.position, emitDir[0], Color.red);
                        checkValidObject(hitInfo, emitDir[0]);
                    }
                }

            }

            //Vertical incoming rays
            else if (ray.z == 10 || ray.z == -10)
            {
                if ((int)transform.eulerAngles.y == 0)
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[2], out hitInfo))
                    {
                        Debug.DrawRay(transform.position, emitDir[2], Color.red);
                        checkValidObject(hitInfo, emitDir[2]);
                    }

                }

                else if ((int)transform.eulerAngles.y == 270)
                {
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[2], out hitInfo))
                    {
                        Debug.DrawRay(transform.position, emitDir[2], Color.red);
                        checkValidObject(hitInfo, emitDir[2]);
                    }
                }

                else if ((int)transform.eulerAngles.y == 90)
                {
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[3], out hitInfo))
                    {
                        Debug.DrawRay(transform.position, emitDir[3], Color.red);
                        checkValidObject(hitInfo, emitDir[3]);
                    }

                }

                else if ((int)transform.eulerAngles.y == 180)
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[3], out hitInfo))
                    {
                        Debug.DrawRay(transform.position, emitDir[3], Color.red);
                        checkValidObject(hitInfo, emitDir[3]);
                    }
                }

            }
        }

        else
        {
            //handle loop condition here
        }
    }
}
