﻿using UnityEngine;
using System.Collections;
using System;

public class SourceSix : MonoBehaviour
{

    [Serializable]
    public class EmitDirection
    {
        public Vector3[] dir = { Vector3.forward, Vector3.left, Vector3.right, Vector3.back, Vector3.up, Vector3.down };

        public RaycastHit[] hitinfo = new RaycastHit[6];
        public LineRenderer[] rayLine = new LineRenderer[6];
        public Transform source;

        public int i;
        public bool[] dirArray = new bool[6];

    }
    //when the puzzle is solved a flag is set via 'destination' game object and particle effect is turned on to emit some particles
    public bool stop = false;
    public GameObject gameObject;
    public Transform randomPipe;
    public EmitDirection emitter = new EmitDirection();
    Transform[] old = new Transform[4];
    //public Material mat = Resources.Load("Laser",typeof(Material)) as Material;

    //Ramdomizer references
    public GameObject[] objectReference = new GameObject[4];
    //Randomizer references

    void Start()
    {
        emitter.rayLine = GetComponentsInChildren<LineRenderer>();

        Material whiteDiff = new Material(Shader.Find("Particles/Additive"));
        for (int i = 0; i < emitter.rayLine.Length; i++)
        {
            emitter.rayLine[i].material = whiteDiff;
            emitter.rayLine[i].SetColors(Color.red, Color.red);
            emitter.rayLine[i].enabled = false;
        }

        old[0] = old[1] = old[2] = old[3] = null;
        Debug.Log(old[0] = this.transform);

        //Rndmzer
        objectReference[0] = GameObject.FindGameObjectWithTag("pipe");
        objectReference[1] = GameObject.FindGameObjectWithTag("elbow");
        objectReference[2] = GameObject.FindGameObjectWithTag("tjoint");
        //Rmdmzer


        //Generate();

    }


    public Vector3 x;
    public int seed, set;
    void Generate()
    {
        /*
        Transform[] old = new Transform[4];
        
        old[0] = old[1] = old[2] = old[3] = null;
        old[0] = this.transform;
        */

        Debug.Log("GEN");

        //RMD
        for (int i = 0; i < seed; i++)
        {

            x.Set(0, 0, UnityEngine.Random.Range(3, 7));
            switch (UnityEngine.Random.Range(1, 3))
            {

                case 1:
                    objectReference[0].transform.position = old[0].transform.position + Vector3.forward + x;
                    Debug.Log(objectReference[0].name);
                    set = 0;
                    break;
                case 2:
                    objectReference[1].transform.position = old[0].transform.position + Vector3.forward + x;
                    Debug.Log(objectReference[1].name);
                    set = 1;
                    break;
                case 3:
                    objectReference[2].transform.position = old[0].transform.position + Vector3.forward + x;
                    Debug.Log(objectReference[2].name);
                    set = 2;
                    break;
            }

            Debug.Log(set);

            old[0] = objectReference[set].transform;
            GameObject.Instantiate(objectReference[set], objectReference[set].transform.position, objectReference[set].transform.rotation);
        }

        //RMD



        /*xxxxxx
        for (int i = 1; i < 4; i++)
        {
            
            if (emitter.dirArray[0])
            {
                UnityEngine.Object z;
                
                x.Set(0, 0, UnityEngine.Random.Range(3, 7));
                
                //Debug.Log(x);
                gameObject.transform.position = old[0].transform.position + Vector3.forward + x;
                //Debug.Log("OLD TRANSFORM: "+gameObject.transform.position);
                //Debug.Log("Random adjust: "+randomPipe.transform.position);
                
                old[0] = gameObject.transform;
                
                //if (i != 1)
                    z = GameObject.Instantiate(gameObject, gameObject.transform.position, gameObject.transform.rotation);
                
            }
            /*yyyyyyyy
            if (emitter.dirArray[1])
                GameObject.Instantiate(gameObject, this.transform.position + Vector3.left * i * 5, gameObject.transform.rotation);
            if (emitter.dirArray[2])
                GameObject.Instantiate(gameObject, this.transform.position + Vector3.right*i*5, gameObject.transform.rotation);
            if (emitter.dirArray[3])
                GameObject.Instantiate(gameObject, this.transform.position + Vector3.back * i * 5, gameObject.transform.rotation);
           yyyyyyyyyy* /
        }
        xxxxx*/
    }

    void FixedUpdate()
    {
        if (!stop)
        {
            for (emitter.i = 0; emitter.i < emitter.dirArray.Length; emitter.i++)
            {
                if (emitter.dirArray[emitter.i])
                {
                    if (Physics.Raycast(transform.position, transform.TransformDirection(emitter.dir[emitter.i]), out emitter.hitinfo[emitter.i]))
                    {
                        Debug.DrawRay(transform.position, emitter.dir[emitter.i]*10, Color.black);
                        emitter.rayLine[emitter.i].SetPosition(0, emitter.source.position);
                        emitter.rayLine[emitter.i].SetPosition(1, emitter.hitinfo[emitter.i].point);
                        emitter.rayLine[emitter.i].enabled = true;


                        if (emitter.hitinfo[emitter.i].collider.tag == "pipe")
                        {
                            emitter.hitinfo[emitter.i].transform.SendMessage("DirectionOfRay", transform.TransformDirection(10*emitter.dir[emitter.i]));
                        }
                        if (emitter.hitinfo[emitter.i].collider.tag == "elbow")
                        {
                            emitter.hitinfo[emitter.i].transform.SendMessage("DirectionOfRay", transform.TransformDirection(10*emitter.dir[emitter.i]));
                        }
                        if (emitter.hitinfo[emitter.i].collider.tag == "tjoint")
                        {
                            emitter.hitinfo[emitter.i].transform.SendMessage("DirectionOfRay", transform.TransformDirection(10*emitter.dir[emitter.i]));
                        }

                    }//Raycast hit check on any object with a collider
                    else
                    {
                        emitter.rayLine[emitter.i].enabled = false;
                    }

                }//Direction emitter check statement
                else
                {
                    emitter.rayLine[emitter.i].enabled = false;
                }
            }//for loop
        }//stop emitting when puzzle is solved
    }//FixedUpdate
}
