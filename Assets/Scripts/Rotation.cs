﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

    public Ray ray;
    public RaycastHit hitInfo;
    private AirCastSource source;
    private Vector3 inputVector = new Vector3(0,0,0);
    private Camera cam;


    private Vector3 t1, t2;
    private float dist;

    void Start()
    {
        dist = Screen.height * 15 / 100;
        source = GameObject.FindObjectOfType<AirCastSource>();
        cam = GetComponent<Camera>();
    }

    public float camSpeed = .5f;


    /*
        void Update()
        {
            if(Input.touchCount>0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Vector2 touchDelta = Input.GetTouch(0).deltaPosition;

                cam.transform.Translate(-touchDelta.x * Time.deltaTime *camSpeed, -touchDelta.y * Time.deltaTime*camSpeed, 0);
            }
        }
    */
    /*
    void Update()
    {
        if(Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began)
            {
                t1 = touch.position;
                t2 = touch.position;
            }
            else if(touch.phase == TouchPhase.Moved)
            {
                t2 = touch.position;
            }
            else if(touch.phase == TouchPhase.Ended)
            {
                t2 = touch.position;
                
                if(Mathf.Abs(t2.x - t1.x)>dist || Mathf.Abs(t2.y - t1.y) > dist)
                {
                    if (Mathf.Abs(t2.x-t1.x) > Mathf.Abs(t2.y - t1.y))
                    {
                        if (t2.x > t1.x)
                            Debug.Log("RS");
                        else
                            Debug.Log("LS");
                    }
                    else
                    {
                        if (t2.y > t1.y)
                            Debug.Log("US");
                        else
                            Debug.Log("DS");
                    }
                }
                else
                {
                    Debug.Log("TAP");
                }
            }
        }
    }
    */

    public GameObject top, front;


    void Update()
    {
        /*222222222222222222222222222222222222222222222222222222*/
        if (Input.GetKeyUp(KeyCode.Keypad2))
        {
            transform.position = front.transform.position;
            transform.rotation = Quaternion.LerpUnclamped(transform.rotation, front.transform.rotation, Time.deltaTime);
        }
        if (Input.GetKeyUp(KeyCode.Keypad8))
        {
            transform.position = top.transform.position;
            transform.rotation = top.transform.rotation;
        }
        /*888888888888888888888888888888888888888888888888888888*/
    }

    void FixedUpdate () {

        if (!source.stop)
        {
            
            if (Input.GetMouseButton(0))
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            else
                ray = new Ray(transform.position, new Vector3(0, 0, 0));
            
            /*
            
            else
                ray = new Ray(transform.position, new Vector3(0, 0, 0));
            */
            if (Physics.Raycast(ray, out hitInfo, 1000))
            {
                if (hitInfo.transform.parent)
                {
                    if (hitInfo.collider.transform.parent.tag == "elbow" || hitInfo.collider.transform.parent.tag == "tjoint")
                    {
                        hitInfo.transform.parent.transform.SendMessage("Rotate");
                    }

                    //test click
                    if(hitInfo.collider.tag == "references")
                    {
                        hitInfo.transform.parent.transform.SendMessage("Rotate");
                    }
                    //test click
                }
                else if (hitInfo.collider.tag == "pipe")
                {
                    hitInfo.transform.SendMessage("Rotate");
                }
            }
        }

    
    }
    
    /*
    void LateUpdate()
    {
        inputVector.x = Input.GetAxisRaw("Horizontal");
        inputVector.z = Input.GetAxisRaw("Vertical");

        inputVector.Normalize();
        inputVector.Scale(new Vector3(.3f, .3f, .3f));
        
        cam.transform.position += inputVector;
    }
    */
}
