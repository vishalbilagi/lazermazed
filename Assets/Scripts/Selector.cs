﻿using UnityEngine;
using System.Collections;

public class Selector : MonoBehaviour {


    public GameObject current, next, temp;
    public RaycastHit hitObj;
    public Vector3 input;
    [SerializeField]
    private bool ready;
	
    void Start()
    {
        current.transform.GetChild(0).gameObject.SetActive(true);
    }
	

	// Update is called once per frame
	void Update () {
        input.x = Input.GetAxis("Horizontal");
        input.z = Input.GetAxis("Vertical");

        
        Debug.DrawRay(current.transform.position, input * 100, Color.blue);
        if (Physics.Raycast(current.transform.position, input, out hitObj, 1000f))
        {
            if (hitObj.collider.tag == "references")
            {
                //Debug.Log(hitObj.transform.GetChild(0).gameObject);
                temp = hitObj.transform.GetChild(0).gameObject;
                //Delete this ↑ if ↓ this works
                //hitObj.transform.SendMessage("Activate",transform.TransformDirection(input.x,0,input.z));
                //if(temp.activeSelf == false)
                temp.SetActive(true);
            }
            else
            {
                temp.SetActive(false);
            }
            if (hitObj.transform.tag != "bound" && hitObj.transform.tag != "unlocker" && Input.GetButtonDown("Fire2"))
            {
                next = hitObj.transform.gameObject;
                current = next;
                //StartCoroutine(ObjectChange());
            }
            Debug.Log(hitObj.transform.tag);
        }
        else
        {
            if (Input.GetButton("Fire1"))
            {
                current.transform.parent.SendMessage("Rotate");
                ready = true;
            }
            temp.SetActive(false);
        }
        /*
        else
        {
            //temp.SetActive(false);
        }
       */

        

    }

    IEnumerator ObjectChange()
    {
        yield return new WaitForSeconds(2);
        ready = true;
        
    }
}
