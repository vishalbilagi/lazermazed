﻿using UnityEngine;
using System.Collections;

public class Glow : MonoBehaviour {

    public Material[] glow;
    public float emission;
    public Color st,final;
	// Use this for initialization
	void Start () {
        glow = GetComponent<MeshRenderer>().materials;
        glow[0].SetColor("_Color", Color.grey);
        glow[0].SetColor("_EmissionColor", Color.black);
        glow[0].SetFloat("_EmmisionScaleUI", 0f);
        glow[1].SetColor("_Color", Color.yellow);
        glow[1].SetColor("_EmissionColor", Color.yellow);
	}
	
	// Update is called once per frame
	void Update () {
        emission = Mathf.PingPong(Time.time*1.25f, .6f);
        st = Color.yellow;

        final = st * Mathf.LinearToGammaSpace(emission);
        glow[1].SetColor("_EmissionColor", final);
	}
}
