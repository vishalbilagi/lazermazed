﻿using UnityEngine;
using System.Collections;

public class SelectionActivator : MonoBehaviour {


    private bool gettingHit;
    void Start()
    {
        gettingHit = false;
    }

    void Update()
    {
        if (gettingHit)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    void Activate(Vector3 input)
    {
        Debug.Log(input);
        if (input.x > 0 || input.z > 0 || input.x < 0 || input.z < 0)
            gettingHit = true;
        else
            gettingHit = false;
    }
}
