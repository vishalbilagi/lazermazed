﻿using UnityEngine;
using System;
using System.Collections;

public class HitReceiver : MonoBehaviour {

    public RaycastHit hitInfo;
    
    /* USAGE : add the objects that are either destination or 
       unlock plates to unlocker script reference object in the inspector panel
    */
    public Unlocker unlocker;

    public Transform pos;

    public LineRenderer pipeRay;
    
    public bool flag = false,rot = false;

    void Start()
    {
        if (!unlocker)
        {
            flag = true;
        }
        pipeRay = GetComponent<LineRenderer>();
        pipeRay.material = new Material(Shader.Find("Particles/Additive"));
        pipeRay.SetColors(Color.red, Color.red);
        pos = GetComponent<Transform>();
        pipeRay.SetPosition(0, pos.transform.position);
        pipeRay.enabled = false;

    }
    

    

    void Update()
    {

        /**
         * X axis test
         * **/
        /*

        int cx, cy;
        cx = (int)transform.eulerAngles.x;
        cy = (int)transform.eulerAngles.y;
        if (rot)
        {

            Quaternion q = Quaternion.AngleAxis(91, Camera.main.transform.forward);
            
            if ((int)transform.eulerAngles.z == 0 || (int)transform.eulerAngles.z < 90)
            {
                //transform.rotation = q* transform.rotation;
                //transform.rotation = q * Quaternion.Lerp(this.transform.rotation, transform.rotation, 8f * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(cx, cy, 91), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.z == 90) rot = false;
            }
            else if ((int)transform.eulerAngles.z == 90 || (int)transform.eulerAngles.z < 180)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(cx, cy, 181), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.z == 180) rot = false;
            }
            else if ((int)transform.eulerAngles.z == 180 || (int)transform.eulerAngles.z < 270)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(cx, cy, 271), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.z == 270) rot = false;
            }
            else if ((int)transform.eulerAngles.z == 270 || (int)transform.eulerAngles.z < 360)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(cx,cy,1), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.z == 0) rot = false;
            }
            
        }
       /* */

        /**/
        if (rot)
        {
            if ((int)transform.eulerAngles.y == 0 || (int)transform.eulerAngles.y < 90)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 91, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 90) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 90 || (int)transform.eulerAngles.y < 180)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 181, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 180) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 180 || (int)transform.eulerAngles.y < 270)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 271, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 270) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 270 || (int)transform.eulerAngles.y < 360)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 1, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 0) rot = false;
            }

        }
        /**/
        
    }



    void Rotate()
    {
        rot = true;

        pipeRay.enabled = false;
        if (hitInfo.transform && hitInfo.collider.tag != "bound" && hitInfo.collider.tag != "dest" && hitInfo.collider.tag != "blocker")
            hitInfo.transform.SendMessage("DisableEmission");

        
    }

    void DisableEmission()
    {
        pipeRay.enabled = false;

        if (hitInfo.transform && hitInfo.collider.tag != "bound" && hitInfo.collider.tag != "dest" && hitInfo.collider.tag != "blocker")
            hitInfo.transform.SendMessage("DisableEmission");

    }

    void checkValidObject(RaycastHit hitInfo, Vector3 dir)
    {

        pipeRay.SetPosition(1, hitInfo.point);
        pipeRay.enabled = true;

        if (hitInfo.collider.tag == "pipe" || hitInfo.collider.tag == "elbow" || hitInfo.collider.tag == "tjoint")
        {
            hitInfo.transform.SendMessage("DirectionOfRay", dir);
        }
        
        if ((hitInfo.collider.tag == "unlocker" || hitInfo.collider.tag == "dest") && !flag)
        {
            if (dir == new Vector3(0, 0, 10))
                unlocker.dirsToAccept[0] = true;
            else if (dir == new Vector3(0, 0, -10))
                unlocker.dirsToAccept[1] = true;
            else if (dir == new Vector3(-10, 0, 0))
                unlocker.dirsToAccept[2] = true;
            else if (dir == new Vector3(10, 0, 0))
                unlocker.dirsToAccept[3] = true;
        }
        if ((hitInfo.collider.tag == "unlocker" || hitInfo.collider.tag == "dest") && flag)
        {
            Debug.Log("You may want to assign an Unlocker gameobject to " + this.gameObject.name);
        }

    }

    void DirectionOfRay(Vector3 ray)
    {
       
        if (ray.x == 10)
        {
            if((int)transform.eulerAngles.y == 0)
            {
                if (Physics.Raycast(transform.GetChild(1).position, ray, out hitInfo))
                {
                    Debug.DrawRay(transform.GetChild(1).position, ray, Color.green);
                    checkValidObject(hitInfo, ray);
                }
                
            }

            if((int)transform.eulerAngles.y == 180)
            {
                if (Physics.Raycast(transform.GetChild(0).position, ray, out hitInfo))
                {
                    Debug.DrawRay(transform.GetChild(0).position, ray, Color.green);
                    checkValidObject(hitInfo, ray);
                }

            }

        }

        else if (ray.x == -10)
        {
            if ((int)transform.eulerAngles.y == 0)
            {
                if (Physics.Raycast(transform.GetChild(0).position, ray, out hitInfo))
                {
                    Debug.DrawRay(transform.GetChild(0).position, ray, Color.green);
                    checkValidObject(hitInfo, ray);
                }
            }

            if ((int)transform.eulerAngles.y == 180)
            {
                if (Physics.Raycast(transform.GetChild(1).position, ray, out hitInfo))
                {
                    Debug.DrawRay(transform.GetChild(1).position, ray, Color.green);
                    checkValidObject(hitInfo, ray);
                }

            }


        }

        else if(ray.z == 10)
        {
            if((int)transform.eulerAngles.y == 90)
            {
                if(Physics.Raycast(transform.GetChild(0).position, ray, out hitInfo))
                {
                    Debug.DrawRay(transform.GetChild(0).position, ray, Color.green);
                    checkValidObject(hitInfo, ray);
                }
            }

            if ((int)transform.eulerAngles.y == 270)
            {
                if (Physics.Raycast(transform.GetChild(1).position, ray, out hitInfo))
                {
                    Debug.DrawRay(transform.GetChild(1).position, ray, Color.green);
                    checkValidObject(hitInfo, ray);
                }
            }


        }

        else if(ray.z == -10)
        {
            if ((int)transform.eulerAngles.y == 90)
            {
                if (Physics.Raycast(transform.GetChild(1).position, ray, out hitInfo))
                {
                    Debug.DrawRay(transform.GetChild(1).position, ray, Color.green);
                    checkValidObject(hitInfo, ray);
                }
            }

            if ((int)transform.eulerAngles.y == 270)
            {
                if (Physics.Raycast(transform.GetChild(0).position, ray, out hitInfo))
                {
                    Debug.DrawRay(transform.GetChild(0).position, ray, Color.green);
                    checkValidObject(hitInfo, ray);
                }
            }
        }
    }  
}
