﻿using UnityEngine;
using System.Collections;

public class CamController : MonoBehaviour {

    
    Camera cam;
    public Transform target;
    public Vector3 input;
    
    void Start () {
        cam = Camera.main;
        
        target = GameObject.FindGameObjectWithTag("target").GetComponent<Transform>();
        cam.transform.LookAt(target.transform.position);
	}


    void Update () {
        input.y = Input.GetAxis("Horizontal");
        input.Normalize();
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow))
        {
            //cam.transform.RotateAround(target.transform.position, -input,  100f* Time.deltaTime);
        }
        if (Input.GetAxisRaw("Horizontal")>0 || Input.GetAxisRaw("Horizontal") < 0)
        {
            cam.transform.RotateAround(target.transform.position, -input, 100f * Time.deltaTime);
        }
        /*
        else
        {
            if (cam.transform.rotation.eulerAngles.y > 0 && cam.transform.rotation.eulerAngles.y < 44)
            {
                cam.transform.RotateAround(target.transform.position, new Vector3(0, -1, 0), 75f * Time.deltaTime);
            }
            else if (cam.transform.rotation.eulerAngles.y > 45 && cam.transform.rotation.eulerAngles.y < 89)
            {
                cam.transform.RotateAround(target.transform.position, new Vector3(0, 1, 0), 75f * Time.deltaTime);
            }
            else if (cam.transform.rotation.eulerAngles.y > 90 && cam.transform.rotation.eulerAngles.y < 134)
            {
                cam.transform.RotateAround(target.transform.position, new Vector3(0, -1, 0), 75f * Time.deltaTime);
            }
            else if (cam.transform.rotation.eulerAngles.y > 135 && cam.transform.rotation.eulerAngles.y < 179)
            {
                cam.transform.RotateAround(target.transform.position, new Vector3(0, 1, 0), 75f * Time.deltaTime);
            }
            else if (cam.transform.rotation.eulerAngles.y > 180 && cam.transform.rotation.eulerAngles.y < 224)
            {
                cam.transform.RotateAround(target.transform.position, new Vector3(0, -1, 0), 75f * Time.deltaTime);
            }
            else if (cam.transform.rotation.eulerAngles.y > 225 && cam.transform.rotation.eulerAngles.y < 269)
            {
                cam.transform.RotateAround(target.transform.position, new Vector3(0, 1, 0), 75f * Time.deltaTime);
            }
            else if (cam.transform.rotation.eulerAngles.y > 270 && cam.transform.rotation.eulerAngles.y < 314)
            {
                cam.transform.RotateAround(target.transform.position, new Vector3(0, -1, 0), 75f * Time.deltaTime);
            }
            else if (cam.transform.rotation.eulerAngles.y > 315 && cam.transform.rotation.eulerAngles.y < 359)
            {
                cam.transform.RotateAround(target.transform.position, new Vector3(0, 1, 0), 75f * Time.deltaTime);
            }
        }
        */
        Debug.Log(cam.transform.rotation.eulerAngles.y);
    }
}
