﻿using UnityEngine;
using System.Collections;

public class HitTjoint : MonoBehaviour {
    
    public RaycastHit hitInfo1, hitInfo2;

     /* USAGE : add the objects that are either destination or 
       unlock plates to unlocker script reference object in the inspector panel
    */
    public Unlocker unlocker;

    public Transform pos;
    public LineRenderer[] pipeRay = new LineRenderer[3];

    public int[] noRays = new int[4];
    public int i;

    public int currentAngleY;

    public bool flag = false, rot = false;
    public Vector3[] emitDir = {new Vector3(0, 0, 10),//up
                                new Vector3(0, 0, -10),//down
                                new Vector3(-10, 0, 0),//left
                                new Vector3(10, 0, 0)};//right

    void Start()
    {
        if (!unlocker)
        {
            flag = true;
        }
        for (int i = 0; i < 4; i++)
            noRays[i] = 0;

        pos = GetComponent<Transform>();
        pipeRay = GetComponentsInChildren<LineRenderer>();
        for (int i = 0; i < pipeRay.Length; i++)
        {            
            pipeRay[i].material = new Material(Shader.Find("Unlit/Color"));
            pipeRay[i].SetColors(Color.red, Color.red);
            pipeRay[i].material.SetColor("_Color", Color.red);
            pipeRay[i].SetPosition(0, pos.transform.position);
            pipeRay[i].enabled = false;
        }
    }

    void Update()
    {
        
        if (rot)
        {
            if ((int)transform.eulerAngles.y == 0 || (int)transform.eulerAngles.y < 90)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 91, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 90) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 90 || (int)transform.eulerAngles.y < 180)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 181, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 180) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 180 || (int)transform.eulerAngles.y < 270)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 271, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 270) rot = false;
            }
            else if ((int)transform.eulerAngles.y == 270 || (int)transform.eulerAngles.y < 360)
            {
                transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 1, 0), 8f * Time.deltaTime);
                if ((int)transform.eulerAngles.y == 0) rot = false;
            }

        }
    }

    void Rotate()
    {
        
        if (hitInfo1.transform && hitInfo1.collider.tag != "bound" && hitInfo1.collider.tag != "dest" && hitInfo1.collider.tag != "blocker")
        {
            hitInfo1.transform.SendMessage("DisableEmission");
        }

        if (hitInfo2.transform && hitInfo2.collider.tag != "bound" && hitInfo2.collider.tag != "dest" && hitInfo2.collider.tag != "blocker")
        {
            hitInfo2.transform.SendMessage("DisableEmission");
        }


        rot = true;
        
        for (int i = 0; i < pipeRay.Length; i++)
        {
            pipeRay[i].enabled = false;
        }
        
    }


    
    void checkValidObject(RaycastHit hitInfo, Vector3 dir, int child)
    {
        
        pipeRay[child].SetPosition(1, hitInfo.point);
        pipeRay[child].enabled = true;

        if (hitInfo.collider.tag == "pipe" || hitInfo.collider.tag == "elbow" || hitInfo.collider.tag == "tjoint")
            hitInfo.transform.SendMessage("DirectionOfRay", dir);
        
        if ((hitInfo.collider.tag == "unlocker" || hitInfo.collider.tag == "dest") && !flag)
        {
            if (dir == new Vector3(0, 0, 10))
                unlocker.dirsToAccept[0] = true;
            else if (dir == new Vector3(0, 0, -10))
                unlocker.dirsToAccept[1] = true;
            else if (dir == new Vector3(-10, 0, 0))
                unlocker.dirsToAccept[2] = true;
            else if (dir == new Vector3(10, 0, 0))
                unlocker.dirsToAccept[3] = true;
        }
        if ((hitInfo.collider.tag == "unlocker" || hitInfo.collider.tag == "dest") && flag)
        {
            Debug.Log("You may want to assign an Unlocker gameobject to " + this.gameObject.name);
        }
        if (hitInfo.collider.tag == "blocker")
        {
            hitInfo.transform.parent.transform.SendMessage("LoopCheck");
        }
    }

    void DisableEmission()
    {
        for (int i = 0; i < pipeRay.Length; i++)
        {
            pipeRay[i].enabled = false;
        }
            
        if (hitInfo1.transform && hitInfo1.collider.tag != "bound" && hitInfo1.collider.tag != "dest" && hitInfo1.collider.tag != "blocker")
        { 
            hitInfo1.transform.SendMessage("DisableEmission");
        }

        if (hitInfo2.transform && hitInfo2.collider.tag != "bound" && hitInfo2.collider.tag != "dest" && hitInfo2.collider.tag != "blocker")
        {
            hitInfo2.transform.SendMessage("DisableEmission");
        }

    }

    void LoopCheck()
    {
        for (i = 0; i < 4; i++)
        {
            noRays[i] = 0;
        }
    }

    void DirectionOfRay(Vector3 ray)
    {
        if (ray == emitDir[0])
            noRays[0] = 1;
        else if (ray == emitDir[1])
            noRays[1] = 1;
        else if (ray == emitDir[2])
            noRays[2] = 1;
        else if (ray == emitDir[3])
            noRays[3] = 1;
        

        if (noRays[0] + noRays[1] + noRays[2] + noRays[3] == 1)
        {
            //Horizontal incoming rays from left

            if (ray.x == 10)
            {
                /*1  0d
                    →[0][e][1]→
                        [2]
                         ↓
                */
                if ((int)transform.eulerAngles.y == 0)
                {
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[3], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[3], Color.blue);
                        checkValidObject(hitInfo1, emitDir[3],1);
                    }
                    if (Physics.Raycast(transform.GetChild(2).position, emitDir[1], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(2).position, emitDir[1], Color.blue);
                        checkValidObject(hitInfo2, emitDir[1],2);
                    }

                }

                /*3  90d
                            ↑
                        [0]
                    →[2][e]
                        [1]
                            ↓
                */
                if ((int)transform.eulerAngles.y == 90)
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[0], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(0).position, emitDir[0], Color.blue);
                        checkValidObject(hitInfo1, emitDir[0],0);
                    }
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[1], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[1], Color.blue);
                        checkValidObject(hitInfo2, emitDir[1],1);
                    }
                }

                /*iv 270 blocks when ray is cast from left
                    [2]
                    →x[e][3]
                    [1]
                */

                /*5  180d
                         ↑
                        [2]                
                    →[1][e][0]→
                */
                if ((int)transform.eulerAngles.y == 180)
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[3], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(0).position, emitDir[3], Color.blue);
                        checkValidObject(hitInfo1, emitDir[3],0);
                    }
                    if (Physics.Raycast(transform.GetChild(2).position, emitDir[0], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(2).position, emitDir[0], Color.blue);
                        checkValidObject(hitInfo2, emitDir[0],2);
                    }
                }
            }
            else if (ray.x == -10)////Horizontal incoming rays from right
            {

                /*2  0d
                    ←[0][e][1]←
                        [2]
                         ↓
                */
                if ((int)transform.eulerAngles.y == 0)//works
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[2], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(0).position, emitDir[2], Color.blue);
                        checkValidObject(hitInfo1, emitDir[2],0);
                    }
                    if (Physics.Raycast(transform.GetChild(2).position, emitDir[1], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(2).position, emitDir[1], Color.blue);
                        checkValidObject(hitInfo2, emitDir[1],2);
                    }
                }

                /*iii  90d blocks when ray is cast from right
                        [0]
                        [2][e]x←
                        [1]
                */
                /*4 270
                     ↑
                    [1]
                    [e][2]←
                    [0]
                     ↓
                */
                if ((int)transform.eulerAngles.y == 270)//works
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[1], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(0).position, emitDir[1], Color.blue);
                        checkValidObject(hitInfo1, emitDir[1],0);
                    }
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[0], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[0], Color.blue);
                        checkValidObject(hitInfo2, emitDir[0],1);
                    }
                }

                /*6  180d
                         ↑
                        [2]                
                    ←[1][e][0]←
                */
                if ((int)transform.eulerAngles.y == 180)//works
                {
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[2], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[2], Color.blue);
                        checkValidObject(hitInfo1, emitDir[2],1);
                    }
                    if (Physics.Raycast(transform.GetChild(2).position, emitDir[0], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(2).position, emitDir[0], Color.blue);
                        checkValidObject(hitInfo2, emitDir[0],2);
                    }
                }


            }
            //Vertical incoming rays

            //Vertical ray incoming form bottom
            else if (ray.z == 10)
            {
                /*1 0d
                    ←[0][e][1]→
                        [2]
                         ↑
                */
                if ((int)transform.eulerAngles.y == 0)//works
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[2], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(0).position, emitDir[2], Color.blue);
                        checkValidObject(hitInfo1, emitDir[2],0);
                    }
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[3], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[3], Color.blue);
                        checkValidObject(hitInfo2, emitDir[3],1);
                    }
                }

                /*2 90d
                     ↑
                    [0]
                ←[2][e]
                    [1]
                     ↑
                */

                if ((int)transform.eulerAngles.y == 90)//works
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[0], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(0).position, emitDir[0], Color.blue);
                        checkValidObject(hitInfo1, emitDir[0],0);
                    }
                    if (Physics.Raycast(transform.GetChild(2).position, emitDir[2], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(2).position, emitDir[2], Color.blue);
                        checkValidObject(hitInfo2, emitDir[2],2);
                    }
                }

                /*ii 180d
                           [2]
                        [1][e][0]
                            x
                            ↑
                */

                /*3 270d
                     ↑
                    [1]
                    [e][2]→
                    [0]
                     ↑
                */
                if ((int)transform.eulerAngles.y == 270)
                {
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[0], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[0], Color.blue);
                        checkValidObject(hitInfo1, emitDir[0],1);
                    }
                    if (Physics.Raycast(transform.GetChild(2).position, emitDir[3], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(2).position, emitDir[3], Color.blue);
                        checkValidObject(hitInfo2, emitDir[3],2);
                    }
                }
            }

            //Vertical ray incoming form top
            else if (ray.z == -10)
            {
                /*i 0d
                        ↓
                        x
                    [0][e][1]
                       [2]
                */

                /*4 90d
                     ↓
                    [0]
                ←[2][e]
                    [1]
                     ↓
                */
                if ((int)transform.eulerAngles.y == 90)//works
                {
                    if (Physics.Raycast(transform.GetChild(2).position, emitDir[2], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(2).position, emitDir[2], Color.blue);
                        checkValidObject(hitInfo1, emitDir[2],2);
                    }
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[1], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[1], Color.blue);
                        checkValidObject(hitInfo2, emitDir[1],1);
                    }
                }
                /*5 180d
                         ↓
                        [2]
                    ←[1][e][0]→
                */
                if ((int)transform.eulerAngles.y == 180)//works
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[3], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(0).position, emitDir[3], Color.blue);
                        checkValidObject(hitInfo1, emitDir[3],0);
                    }
                    if (Physics.Raycast(transform.GetChild(1).position, emitDir[2], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(1).position, emitDir[2], Color.blue);
                        checkValidObject(hitInfo2, emitDir[2],1);
                    }
                }
                /*6 270d
                     ↓
                    [1]
                    [e][2]→
                    [0]
                     ↓
                */
                if ((int)transform.eulerAngles.y == 270)
                {
                    if (Physics.Raycast(transform.GetChild(0).position, emitDir[1], out hitInfo1))
                    {
                        Debug.DrawRay(transform.GetChild(0).position, emitDir[1], Color.blue);
                        checkValidObject(hitInfo1, emitDir[1],0);
                    }
                    if (Physics.Raycast(transform.GetChild(2).position, emitDir[3], out hitInfo2))
                    {
                        Debug.DrawRay(transform.GetChild(2).position, emitDir[3], Color.blue);
                        checkValidObject(hitInfo2, emitDir[3],0);
                    }
                }
            }
        }
        else
        {
            //handle loop condition here
        }  
    }
}
